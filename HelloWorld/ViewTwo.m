//
//  ViewTwo.m
//  HelloWorld
//
//  Created by Abhijit Fulsagar on 1/26/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ViewTwo :UIViewController{
    IBOutlet UILabel *message1;
    IBOutlet UIButton *change;
    IBOutlet UITextField *textfield;
}
@end

@implementation ViewTwo

-(IBAction)change:(id)sender
{
    message1.text=textfield.text;
}
@end
