//
//  ViewController.h
//  HelloWorld
//
//  Created by Abhijit Fulsagar on 1/22/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UILabel *message;

    IBOutlet UIButton *show;
    IBOutlet UIButton *hide;
    IBOutlet UIButton *reset;
    
}
@end

