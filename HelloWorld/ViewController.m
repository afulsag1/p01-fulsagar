//
//  ViewController.m
//  HelloWorld
//
//  Created by Abhijit Fulsagar on 1/22/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
-(IBAction)show:(id)sender{
    message.hidden=NO;
    hide.hidden=NO;
    message.text=@"My name is Abhijit Fulsagar";
}
-(IBAction)hide:(id)sender{
    message.hidden=YES;
    hide.hidden=YES;
}
-(IBAction)reset:(id)sender
{
    message.hidden=NO;
    message.text=@"HelloWorld";
    hide.hidden=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
