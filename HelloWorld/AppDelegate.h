//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Abhijit Fulsagar on 1/22/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

